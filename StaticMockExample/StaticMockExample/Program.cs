﻿using NUnit.Framework;
using StaticMock;

namespace MyNamespace
{

    [TestFixture]
    public class TestSMock
    {
        [Test]
        public void TestMockEmb()
        {
            // arrange
            Mock.Setup(typeof(Callee), nameof(Callee.Call2), () =>
            {
                Mock.Setup(typeof(CalleeNext), nameof(CalleeNext.Call3), () =>
                {
                    // act
                    var mockActualResult = Caller.Call1(0);

                    // assert
                    Assert.AreEqual(26, mockActualResult);
                }).Returns(15);
            }).Returns(11);
        }
        
        [Test]
        public void TestMockSeq()
        {
            // arrange
            Mock.Setup(typeof(Callee), nameof(Callee.Call2)).Returns(15);
            Mock.Setup(typeof(CalleeNext), nameof(CalleeNext.Call3)).Returns(11);
            
            // act
            var mockActualResult = Caller.Call1(0);

            // assert
            Assert.AreEqual(26, mockActualResult);
        }
    }
}