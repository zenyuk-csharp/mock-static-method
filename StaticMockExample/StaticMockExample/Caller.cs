namespace MyNamespace
{
    public class Caller
    {
        public static int Call1(int x)
        {
            x++;
            int r = Callee.Call2(x);
            r += CalleeNext.Call3(x);
            return r;
        }
    }
}